#include "vec.h"

struct Box {
  Point min ;
  Point max ;

  float hit(const Point& origin, const Vector& direction) ;
} ;
